@extends('templates/base')

@include('components/navbar')
@include('components/sidebar')

@section('container')
  <div class="content right" id="autorPage">
    <div class="fix-space">

      @if ($action == 1)
          <form class="form" action="{{ route('students.store') }}" method="post">
      @else
          <form class="form" action="{{ route('students.update', $reg->id) }}" method="post">
          {!! method_field('put') !!}
      @endif      
        {{ csrf_field() }}
        
        <div class="line">
          <label class="lbl" for="nome">Nome:</label>
          <input type="text" class="ipt-text" id="nome" name="nome" value="{{ $reg->nome or old('nome') }}">
        </div>
        <div class="line">
          <label class="lbl" for="email">E-mail:</label>
          <input type="text" class="ipt-text" id="email" name="email" value="{{ $reg->email or old('email') }}">
        </div>
        <div class="line">
          <label class="lbl" for="endereco">Endereço:</label>
          <input type="text" class="ipt-text" id="endereco" name="endereco" value="{{ $reg->endereco or old('endereco') }}">
        </div>
        <div class="line">
          <label class="lbl" for="cep">CEP:</label>
          <input type="text" class="ipt-text" id="cep" name="cep" value="{{ $reg->cep or old('cep') }}">
        </div>
        <div class="line">
          <label class="lbl" for="telefone1">Telefone 1:</label>
          <input type="text" class="ipt-text" id="telefone1" name="telefone1" value="{{ $reg->telefone1 or old('telefone1') }}">
        </div>
        <div class="line">
          <label class="lbl" for="telefone2">Telefone 2:</label>
          <input type="text" class="ipt-text" id="telefone2" name="telefone2" value="{{ $reg->telefone2 or old('telefone2') }}">
        </div>
        <div class="line">
          <label class="lbl" for="rf">RG:</label>
          <input type="text" class="ipt-text" id="rg" name="rg" value="{{ $reg->rg or old('rg') }}">
        </div>
        <div class="line">
          <label class="lbl" for="cpf">CPF:</label>
          <input type="text" class="ipt-text" id="cpf" name="cpf" value="{{ $reg->cpf or old('cpf') }}">
        </div>

        <div class="line">
          <label class="lbl" for="idCurso" id="curso">Curso</label>
          <select class="ipt-text select" id="idCurso" name="idCurso">
            <option value="0">Selecione</option>
            @foreach ($courses as $c)
              <option value="{{ $c->id }}" @if ((isset ($reg) and $c->id == $reg->idCurso) or old('idCurso') == $c->id) selected @endif>{{ $c->nome }}</option>
            @endforeach
          </select>
        </div>

        <div class="line">
          <label class="lbl" for="idTurno" id="turno">Turno</label>
          <select class="ipt-text select" id="idTurno" name="idTurno">
            <option value="0">Selecione</option>
            @foreach ($shifts as $t)
              <option value="{{ $t->id }}" @if ((isset ($reg) and $t->id == $reg->idTurno) or old('idTurno') == $t->id) selected @endif>{{ $t->nome }}</option>
            @endforeach
          </select>
        </div>

        <div class="line">
          <label class="lbl" for="comissao">Comissão:</label>
          <input type="radio" class="ipt-text" id="comissao" name="comissao" value="{{ $reg->turno or old('turno') }}">
        </div>

        <div class="buttons-list">
          @if ($action == 2)
            <form style="display: inline-block;" method="POST" action="{{ route('students.destroy', $reg->id) }}">
              {{ method_field('DELETE') }}
              {{ csrf_field() }}

              <button type="submit" class="buttons-list__item remove">Remover</button>
            </form>
          @endif
          <a href="/books" class="buttons-list__item" id="cancelar">Cancelar</a><!--
          --><input type="submit" class="buttons-list__item" value="Salvar" id="salvar">
        </div>
      </form>

    </div>
  </div>
@endsection