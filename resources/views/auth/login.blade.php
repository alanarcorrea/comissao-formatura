@extends('../templates/base')

@section('container')

<section class="login">

    <div class="box box-login">
        <h2 class="title">Login</h2>

        <form class="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="line">
                <label for="email" class="lbl">Email</label>
                <input type="text" name="email" id="email" class="text icon icon-email">
                <span class="active-line"></span>
            </div>

            <div class="line">
                <label for="password" class="lbl">Senha</label>
                <input type="password" name="password" id="password" class="text icon icon-locked">
                <span class="active-line"></span>
            </div>

            <input type="submit" class="button gradient-green" value="Login">
        </form>

        <a href="javascript:;" class="forgot-password">Esqueceu sua senha?</a>
    </div>

</section>

<script src="{{ asset('js/pages/login.js') }}"></script>

@endsection