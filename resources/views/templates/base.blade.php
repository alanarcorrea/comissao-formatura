<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Comissão de Formatura</title>

    <!-- Bootstrap core CSS -->
    {{Html::style('vendor/bootstrap/css/bootstrap.min.css')}}

    <!-- Custom fonts for this template -->
    {{Html::style('vendor/font-awesome/css/font-awesome.min.css')}}
    {{Html::style('css/style.css')}}

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <script src="{{ asset('/js/lib/jquery.min.js') }}"></script>
  </head>

    <body id="page-top">

        @yield('container')

        

    </body>
</html>