<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Alana',
            'email' => 'alanarcorrea@gmail.com',
            'password' => bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Yasmin',
            'email' => 'yasmin@gmail.com',
            'password' => bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'name' => 'Guilherme',
            'email' => 'guilherme@gmail.com',
            'password' => bcrypt('secret')
        ]);
    }
}
