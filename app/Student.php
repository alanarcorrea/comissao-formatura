<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;
    protected $table = 'students';
    protected $fillable = array('comissao','nome', 'email', 'endereco', 'cep', 'telefone1', 'telefone2', 'rg', 'cpf','idCurso', 'idTurno');
}
