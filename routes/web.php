<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('students', 'StudentsController');
Route::resource('financial', 'FinancialsController');
Route::resource('documents', 'DocumentsController');
Route::resource('dashboard', 'DashboardController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('register', function(){
  //  return '<h1>Acesso restrito! </h1>';
//});



