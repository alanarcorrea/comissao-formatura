@extends('templates/base')

@include('components/navbar')
@include('components/sidebar')

@section('container')

  <div class="content right" id="autorPage">
    <div class="fix-space">

      <div class="main-list">
        <div class="list-item-header">
          <span class="col">Nome</span>
          <span class="col">Curso</span>
          <span class="col">Turno</span>
          <span class="col"></span>
        </div>

        @foreach ($students as $student)
          <a href="students/{{ $student->id }}" class="list-item">
            <span class="col">{{ $student->nome }}</span>
            <span class="col">{{ $student->curso }}</span>
            <span class="col">{{ $student->turno }}</span>
            <span class="col"></span>
          </a>
        @endforeach        
      </div>

      <div class="buttons-list">
        <a href="/registration" class="buttons-list__item" id="voltar">Voltar</a>
        <a href="{{ route('students.create') }}" class="buttons-list__item" id="novo">Novo</a>
      </div>
    </div>
  </div>

@endsection
