<div class="sidebar left">
    <div class="menu">
        <a href="{{ url('/dashboard') }}" class="menu__item menu-dashboard" id="sidebarDashboard">Dashboard</a>
        <a href="{{ url('/students') }}" class="menu__item menu-registration" id="sidebarCadastros">Formandos</a>
        <a href="{{ url('/financial') }}" class="menu__item menu-lending" id="sidebarEmprestimo">Financeiro</a>
        <a href="{{ url('/documents') }}" class="menu__item menu-payment" id="sidebarPagamento">Documentos</a>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="menu__item" id="logoutButton">Sair</a>
    </div>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>