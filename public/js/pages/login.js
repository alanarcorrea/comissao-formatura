ST = {
    init: function() {
        this.binds();
    },

    binds: function() {
        $('.box-login .text').on('focusin', this.hoverInput.bind(this));
        $('.box-login .text').on('focusout', this.hoverInput.bind(this));
    },

    hoverInput: function(event) {
        var target = $(event.currentTarget);
        var line = target.closest('.line');

        switch (event.type) {
            case 'focusin':
                line.addClass('active');
                break;
            case 'focusout':
                if (target.val() == '') {
                    line.removeClass('active');
                }                
                break;
        }
    }
};

$(document).ready(function() {
    ST.init();
});