<?php

use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documents')->insert([
            'id' => 1,
            'titulo' => 'Contrato',
            'documento' => 'docs'
        ]);
    }
}
