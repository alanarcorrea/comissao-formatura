<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'id' => 1,
            'nome' => 'ADS'
        ]);
        DB::table('courses')->insert([
            'id' => 2,
            'nome' => 'Redes'
        ]);
        DB::table('courses')->insert([
            'id' => 3,
            'nome' => 'Markeging'
        ]);
        DB::table('courses')->insert([
            'id' => 4,
            'nome' => 'Processos Gerenciais'
        ]);
    }
}
