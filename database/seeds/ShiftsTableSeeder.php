<?php

use Illuminate\Database\Seeder;

class ShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shifts')->insert([
            'id' => 1,
            'nome' => 'Manhã'
        ]);
        DB::table('shifts')->insert([
            'id' => 2,
            'nome' => 'Tarde'
        ]);
        DB::table('shifts')->insert([
            'id' => 3,
            'nome' => 'Noite'
        ]);
    }
}
