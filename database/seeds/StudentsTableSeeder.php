<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'id' => 1,
            'comissao' => 1,
            'nome' => 'Alana Ramires Corrêa',
            'email' => 'alanarcorrea@gmail.com',
            'endereco' => 'Rua Abrilino Ferreira Cardoso, 125',
            'cep' => '96020000',
            'telefone1' => '991626869',
            'telefone2' => '981140257',
            'rg' => '1113109126',
            'cpf' => '02964885001',
            'idCurso' => 1,
            'idTurno' => 1
        ]);
    }
}
