<?php

use Illuminate\Database\Seeder;

class FinancialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('financials')->insert([
            'id' => 1,
            'tipo_mov' => 1,
            'valor' => 100.00,
            'descricao' => 'Festa',
            'data' => '2018-08-08'
        ]);
    }
}
