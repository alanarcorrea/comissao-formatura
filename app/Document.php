<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $timestamps = false;
    protected $table = 'documents';
    protected $fillable = array('titulo', 'documento');
}
