<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    public $timestamps = false;
    protected $table = 'financials';
    protected $fillable = array('tipo_mov', 'valor', 'descricao', 'date');
}
