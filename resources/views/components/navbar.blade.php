<nav class="navbar2">
    <div class="col left">
        <a href="{{ url('/dashboard') }}" class="title">Comissão</a>
    </div>

    <div class="col right">
        <span class="section-title left">{{ $title }}</span>
        <a href="javascript:;" class="avatar right"></a>
    </div>
</nav>